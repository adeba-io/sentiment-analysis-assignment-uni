import naive_bayes as nb
import math
import sys

def compute_utility(entries: list, term: str, cls: int) -> float:
	"""
	Implements mutual information to compute the utility of a term to a certain class
	It measures how much the prescence of a term contributes to the classification of the term
	"""
	# N00 = the number of documents that don't contain the term and the doc isn't in the class
	# N01 the number of documents that don't contain the term but the document is in the class
	# N10 the number of documents that contain the term but aren't in the class
	# N11 the number of documents that contain term and the term is in the class
	N = [ [ 1, 1 ],
	      [ 1, 1 ] ]
	N_total = 0
	for entry in entries:
		term_in_doc = 1 if term in entry['phrase'] else 0
		doc_in_class = 1 if cls == entry['class'] else 0

		N_total += 1
		N[term_in_doc][doc_in_class] += 1

	N0_ = N[0][0] + N[0][1]
	N1_ = N[1][0] + N[1][1]
	N_0 = N[0][0] + N[1][0]
	N_1 = N[0][1] + N[1][1]

	log = (N_total * N[1][1]) / (N1_ * N_1)
	a = (N[1][1] / N_total) * math.log(log, 2)

	log = (N_total * N[0][1]) / (N0_ * N_1)
	b = (N[0][1] / N_total) * math.log(log, 2)

	log = (N_total * N[1][0]) / (N1_ * N_0)
	c = (N[1][0] / N_total) * math.log(log, 2)

	log = (N_total * N[0][0]) / (N0_ * N_0)
	d = (N[0][0] / N_total) * math.log(log, 2)

	return a + b + c + d


def select_features(entries: list, num_of_features: int) -> dict:
	# Select features based on the provided training data
	vocabulary = nb.get_vocabulary(entries)
	features = {}
	for cls in vocabulary['classes']:
		utilities = {}
		# Assign each a score in relation to the class, describing the term's
		# usefulness to identifying the class
		for term in vocabulary[cls]['terms']:
			utility = compute_utility(entries, term, cls)
			utilities[term] = utility
		
		# We then rank all the terms based on this usefulness and return the <num_of_features>
		# best variables
		l = sorted(utilities.items(), reverse = True, key = lambda x: x[1])[:num_of_features]
		features[cls] = [ f[0] for f in l ]

	return features

def read_feature_file(path: str) -> dict:
	"""
	A function meant to be used by other modules
	Reads and returns a dictionary of the features in the file at path
	"""
	features = { 'all': set() }
	with open(path, 'r') as f:
		cls = -1
		for line in f:
			line = line.replace('\n', '')
			if len(line) == 0:
				cls = -1
				continue

			if cls == -1:
				cls = int(line)
				features[cls] = set()
			else:
				features['all'].add(line)
				features[cls].add(line)

	return features

def main(train_file: str, n_features: int, feature_out: str, reduce_classes: bool):
	entries = nb.tsv_reader(train_file, reduce_classes)

	features = select_features(entries, n_features)

	with open(feature_out, 'w') as file:
		for cls, terms in features.items():
			file.write(f"{cls}\n")
			for term in terms:
				if term == '':
					continue
				file.write(f"{term}\n")
			file.write('\n')

if __name__ == '__main__':
	args = sys.argv[1:]
	if len(args) < 3:
		print(f'python {sys.argv[0]} <path_to_train_file> <num_of_features> <feature_out> <reduce_classes?>')
	else:
		train_file = args[0]
		n_features = int(args[1])
		feature_out = args[2]
		reduce_classes = len(args) > 3 and list(args[3])[0] == 'y'

		main(train_file, n_features, feature_out, reduce_classes)
