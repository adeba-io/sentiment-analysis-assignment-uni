import sys, re
import naive_bayes as nb
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

def read_model(path: str) -> (dict, dict):
	"""
	Reads the priors and likelihoods from the provided model
	"""
	priors = {}
	likelihoods = {}
	with open(path, 'r') as model:
		reading_priors = True
		read_term_name = True
		curr_term = ''

		for line in model:
			line = line.replace('\n', '')
			if len(line) > 0 and line[0] == '#': continue

			if reading_priors:
				if len(line) == 0:
					reading_priors = False
					continue

				split = line.split(' ')
				priors[int(split[0])] = float(split[1])
			else:
				if len(line) == 0:
					read_term_name = True
					continue
				
				if read_term_name:
					curr_term = line
					likelihoods[curr_term] = { }
					read_term_name = False
				else:
					split = line.split(' ')
					cls = int(split[0])
					if cls not in priors:
						print(f'Error: Class {cls} not in priors')
						continue
					
					likelihoods[curr_term][cls] = float(split[1])

	return priors, likelihoods

def get_name_from_path(path: str) -> str:
	# Simply gets the file name minus the extension
	return path.replace('\\', '/').split('/')[-1].split('.')[0]

def evaluate_models(predictions: list, priors: dict, model_file: str, test_file: str, feature_file: str):
	class_count = len(priors.keys())
	mat_confusion = []
	for i in range(class_count):
		mat_confusion.append([ 0 ] * class_count)

	# Create the confusion matrix
	for pred in predictions:
		actual = pred[1]
		predicted = pred[2]
		mat_confusion[predicted][actual] += 1
	
	name = get_name_from_path(model_file)
	# Using matplotlib and seaborn we create a heat map
	fig = plt.figure(figsize = (10, 9))
	ax = sns.heatmap(mat_confusion, annot = True, fmt = 'd')
	ax.set_title(f"Confusion Matrix on {name}")
	ax.set_ylabel("Predicted Class")
	ax.set_xlabel("Actual Class")

	# Create a unique name using the names of the files used
	# to create the matrix
	name += '_' + get_name_from_path(test_file) + '_'
	if feature_file == '':
		name += 'no_selection'
	else:
		name += get_name_from_path(feature_file)

	# Then save it to a file
	fig.savefig(f"confusions/{name}.png")

	macro_f1 = 0
	for i in range(class_count):
		# Calculate the true positives, true negatives, false positive, false negatives
		tp = mat_confusion[i][i]
		fn = 0
		fp = 0
		tn = 0
		for j in range(class_count):
			if j == i: continue
			fn += mat_confusion[i][j]
			fp += mat_confusion[j][i]
		
			for k in range(class_count):
				if k == i: continue
				tn += mat_confusion[j][k]
		
		# Use the tp, tn, fp and fn values to calculate the accuracy,
		# precision, recall and f1 scores
		accuracy = (tp + tn) / (tp + tn + fp + fn)
		precision = tp / (tp + fp)
		recall = tp / (tp + fn)

		f1 = (2 * tp) / (2 * tp + fp + fn)
		print(f"{i} Scores: F1: {f1} Accuracy: {accuracy} Precision: {precision} Recall: {recall}")
		macro_f1 += f1

	macro_f1 /= class_count
	print(f'Macro F1: {macro_f1}')

def main(model_file: str, test_file: str, feature_file: str, has_classifications: bool):
	# Read the priors and likelihoods from the given model
	priors, likelihoods = read_model(model_file)
	reduce_classes = len(priors.keys()) == 3
	# Read in the data entries from the file to test against
	entries = nb.tsv_reader(test_file, reduce_classes, has_classifications, feature_file)
	
	# Initialise the predictions to be an array the same length as entries
	predictions = [ None ] * len(entries)
	for i, entry in enumerate(entries):
		# We predict the class by multiplying the prior of the given class
		# with the likelihood of all the terms in the phrase
		largest_prob = -1.0
		pred_class = 0
		for cls, prob in priors.items():
			for term in entry['phrase']:
				prob *= likelihoods[term][cls] if term in likelihoods else 0
			
			# We then take the class that has the highest probability
			if prob > largest_prob:
				largest_prob = prob
				pred_class = cls
		
		if has_classifications:
			# We use the provided class later to evaluate the model, if we have it
			predictions[i] = [ entry['sentence_id'], entry['class'], pred_class ]
		else:
			predictions[i] = [ entry['sentence_id'], pred_class ]

	if has_classifications:
		evaluate_models(predictions, priors, model_file, test_file, feature_file)
	else:
		# We assume that if we don't have classifications in the test data
		# we want to export the predictions to a file

		# Read in the destination here rather, to not over crowd the command line
		dest = input('Results destination file: ')
		with open(dest, 'w') as results:
			results.write("SentenceId\tSentiment\n")
			for p in predictions:
				results.write(f'{p[0]}\t{p[1]}\n')

if __name__ == '__main__':
	args = sys.argv[1:]
	if len(args) < 2:
		print(f'python {sys.argv[0]} <path_to_model> <path_to_test_set> <has_classifications?> <path_to_features?>')
	else:
		model_file = args[0]
		test_file = args[1]
		has_classifications = len(args) < 3 or list(args[2])[0] == 'y'
		feature_file = args[3] if len(args) > 3 else ''

		main(model_file, test_file, feature_file, has_classifications)
