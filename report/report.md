# Sentiment Analysis

## Text Processing Assignment 2

Oluwatamilore Adebayo, aca19oaa

## Implementation Description

My classifier comes in the form of three scripts. `feature_selection.py`, `train.py` and `find_results.py`.  

The first script is responsible for what it's name describes: feature selection.
It finds the best terms to be used to differentiate the classes, then 
prints those terms to an output file, specified by the user. 
These terms are picked through the use of the mutual information algorithm.
The mutual information algorithm's goal is evaluate how useful the terms' presence/absence is 
in identifying a class. Terms more important to this are given higher scores.
With this data we can then sort all the features, providing on a certain number from the top.  

The second script called `train.py` is responsibly for generating models to be tested against.
The using training data and an optional provided feature set (perhaps from the previous script) 
it calculates to key bits of data: the priors and the likelihoods.
The priors are the the baseline probability of any document in the collection belonging to a particular 
class. Mathematically, it is simple the count of documents in the class divided by the total number of 
documents in the collection.
The likelihoods are the chance that a given term will appear in a class. Mathematically, this would be 
the number of occurances the term has in the class divided by the total number of terms in the class 
across all documents, including any duplicates.
This how it is implemented naively, however down the line, when I make predictions based on the
models, I will be performing operations that involve multiplying the likelihoods.
Problems can arise from this situation as in the implementation described above, a term that does not
appear in the class would be 0.  
To solve this issue, I used a modified form of the equation. Adding 1 to the numerator and the total number 
of unique terms to the denominator.

The final script, `find_results.py`, is for generating predictions based on the model provided to it.
When provided with a test set that already has the classifications, it will compute a confusion matrix 
then the accuracy, precision and recall values, and finally the F1 and F1-macro scores, which is the average 
F1 score across the classes.

## Results

Macro F1 Scores  
|      |3 class|5 class|
|:-----|:-----:|:-----:|
|No Feature Selection|0.3771|0.2439|
|Feature Selection (30)|0.3633|0.2475|
|Feature Selection (50)|0.3845|0.2831|
|Feature Selection (100)|0.4140|0.3009|
|Feature Selection (500)|0.4943|0.3231|
|Feature Selection (1000)|0.5144|0.3131|

*Macro F1 scores for differenct configurations*

All scores presented above were tested with the models made from `moviereviews/train.tsv` and evaluated on `moviereviews/dev.tsv`.
The feature selections were generated at lengths of 30, 50, 100 and 500.  

From the results of the table, it is clear that the three class models are getting much better scores regardless of the number
of selected features.
What is interesting is that after feature selection was introduced, the three class model saw steady growth, not even 
showing signs of stopping at 1000 features; whereas the five class model would increase yet fluctuate decrease at certain climbs, 
such as from 500 features to 1000.  

Following the data from the table, I have decided to use a feature selection count of 1000 for the 3 class, and a feature selcted count of 500
for the 5 class prediction files of the test and dev corpora.

Given that there are a total of 15'285 unique terms in the collection, it safe to assume that I may be able to increase the feature 
counts quiet a bit before I start seeing diminishing returns.
