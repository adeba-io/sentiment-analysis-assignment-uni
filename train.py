import sys
import naive_bayes as nb

def write_model(path: str, priors: dict, likelihoods: dict):
	"""
	Ouputs the model to a file specified by the path
	"""
	with open(path, 'w') as model:
		model.write('# Priors\n')
		for cls, val in priors.items():
			model.write(f'{cls} {val}\n')
		
		model.write('\n# Likelihoods\n')
		for term, likelies in likelihoods.items():
			model.write(f'{term}\n')
			for cls, val in likelies.items():
				model.write(f'{cls} {val}\n')
			model.write('\n')

def main(train_file: str, feature_file: str, model_out: str, reduce_classes: bool):
	entries = nb.tsv_reader(train_file, reduce_classes, feature_file=feature_file)
	priors = nb.calculate_priors(entries)
	likelihoods = nb.calculate_likelihoods(entries)

	write_model(model_out, priors, likelihoods)

if __name__ == '__main__':
	args = sys.argv[1:]
	if len(args) < 2:
		print(f'python {sys.argv[0]} <path_to_train_file> <model_out> <reduce_classes?> <path_to_feature_file?>')
	else:
		train_file = args[0]
		model_out = args[1]
		reduce_classes = len(args) > 2 and list(args[2])[0] == 'y'
		feature_file = args[3] if len(args) > 3 else ''

		main(train_file, feature_file, model_out, reduce_classes)
