import re
import feature_selection as fs

def get_stopwords() -> set:
	with open('stopwords.txt', 'r') as f:
		return set(f.read().split('\n'))

def tsv_reader(filepath: str, reduce_classes = False, is_classified = True, feature_file = '') -> list:
	entries = []
	stopwords = get_stopwords()
	use_features = feature_file != ''
	features = fs.read_feature_file(feature_file) if use_features else {}

	with open(filepath, 'r') as tsv_file:
		for i, row in enumerate(tsv_file):
			if i == 0: continue

			split = row.split('\t')
			# Split the phrase by white space chars after converting everything to lower case
			phrase = re.split('\s+', split[1].lower())

			if use_features:
				# No need to use the stopwords here as they couldn't be in
				# the selected features
				phrase = [ term for term in phrase if term in features['all'] ]
			else:
				phrase = [ term for term in phrase if term not in stopwords ]

			dict = { 'sentence_id': int(split[0]), 'phrase': phrase }
			# A check to avoid errors if we;re reading from test.tsv
			if is_classified:
				cls = int(split[2])
				if reduce_classes:
					# Transfroms the classes from 5 dimensions to 3
					switcher = { 0: 0, 1: 0, 2: 1, 3: 2, 4: 2 }
					cls = switcher.get(cls, 0)
				dict['class'] = cls
			entries.append(dict)
	
	return entries

def get_vocabulary(entries: list) -> dict:
	"""
	vocabulary {
		terms              the set of all terms in the data
		count              the number of unique terms in the data
		classes            the set of classes in the data
		<class> {
			terms            the set of terms in the class
			count            the number of terms in the class (duplicates counted)
			l_terms          the list of terms in the class (duplicates included)
		}
		term_counts_per_class {
			<term>: <class>  the number of times <term> appears in the class <class>
		}
	}
	"""
	vocabulary = { }
	all_terms = set()
	term_counts_per_class = {}

	for entry in entries:
		cls = entry['class']
		if cls not in vocabulary:
			vocabulary[cls] = []

		for term in entry['phrase']:
			all_terms.add(term)
			vocabulary[cls].append(term)

			if term in term_counts_per_class:
				if cls in term_counts_per_class[term]:
					term_counts_per_class[term][cls] += 1
				else:
					term_counts_per_class[term][cls] = 1
			else:
				term_counts_per_class[term] = { cls: 1 }
	
	for cls, terms in vocabulary.items():
		vocabulary[cls] = { "terms": set(terms), "count": len(terms), "l_terms": terms }

	vocabulary['classes'] = set(vocabulary.keys())
	
	vocabulary["terms"] = all_terms
	vocabulary["count"] = len(all_terms)
	for term in term_counts_per_class:
		for cls in vocabulary['classes']:
			if cls not in term_counts_per_class[term]:
				term_counts_per_class[term][cls] = 0
	vocabulary["term_counts_per_class"] = term_counts_per_class
	return vocabulary


def calculate_priors(entries: list) -> dict:
	"""
	Calculates the priors from the given data
	"""
	priors = { }

	for entry in entries:
		cls = entry['class']
		if cls in priors:
			priors[cls] += 1
		else:
			priors[cls] = 1
	
	total = len(entries)
	for cls, count in priors.items():
		# The prior is simply the ratio of how many times the class appears in the collections
		# over how many entries in the collection in total
		priors[cls] = count / total
	
	return priors

def calculate_likelihoods(entries: list) -> dict:
	"""
	Calculates the likelihoods from the given data, which is just the chance that the term
	is in the given class
	"""
	vocabulary = get_vocabulary(entries)
	likelihoods = {}
	for term, counts in vocabulary["term_counts_per_class"].items():
		# The likelihoods for a given term are a dictionary of the class to the likelihood
		dict = {}
		for cls in vocabulary['classes']:
			# The smoothing function, applied so that classes that do not have the term are not given 0
			dict[cls] = (counts[cls] + 1) / (vocabulary[cls]['count'] + vocabulary['count'])
		likelihoods[term] = dict
	
	return likelihoods
