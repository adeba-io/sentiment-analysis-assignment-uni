# Text Processing Assignment 2

## How to Use

The scripts used are:

- `feature_selection.py`
- `train.py`
- `naive_bayes.py`

Running these scripts with no arguments will tell you what arguments to pass in.
Any arguments ending with a question mark `?` are optional.

Take care to always provide the same feature file to `find_results.py` if you are giving it
a model generating using it. A division  by zero error will be generated otherwise.

## Folders

`models/` is where I have put all the models used to generate the data for the report.
`results/` is where I have put all the prediction of the development and test corpora.
`confusions/` is where you will find all the generated confusion matrixes
